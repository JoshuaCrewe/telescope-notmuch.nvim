-- https://github.com/nvim-telescope/telescope.nvim/blob/master/developers.md

local pickers = require("telescope.pickers")
local finders = require("telescope.finders")
local conf = require("telescope.config").values

local actions = require("telescope.actions")
local action_state = require("telescope.actions.state")

-- using jq
local function getaddresses()
	local file = io.popen(
		"/usr/bin/notmuch address --format=json --deduplicate=address '*' | /usr/bin/jq -r '.[] | .[\"name-addr\"]'"
	)
	local addresses = {}

	if file == nil then
		return addresses
	end

	local output = file:read("*a")
	file:close()

	for address in string.gmatch(output, "([^'\n']+)") do
		table.insert(addresses, address)
	end

	return addresses
end
-- end jq

-- using in built function
-- local function getaddresses()
-- local json = require "lib.json"

-- local stream = io.popen ("/usr/bin/notmuch address --format=json --deduplicate=address '*'")
-- output = stream:read ("*a")
-- stream:close ()

-- local data = json:decode(output)

-- addresses={}

-- for k,v in pairs(data) do
--     table.insert(addresses, v["name-addr"])
-- end
-- end in built
--
-- return addresses

-- for k,v in ipairs(addresses) do
-- print(k..":"..v)
-- end
-- end

-- our picker function: colors
local notmuch = function(opts)
	opts = opts or {}
	pickers
		.new(opts, {
			prompt_title = "contact",
			finder = finders.new_table({
				results = getaddresses(),
			}),
			sorter = conf.generic_sorter(opts),
			attach_mappings = function(prompt_bufnr)
				actions.select_default:replace(function()
					actions.close(prompt_bufnr)
					local selection = action_state.get_selected_entry()
					-- print(vim.inspect(selection))
					vim.api.nvim_put({ selection[1] }, "", true, true)
				end)
				return true
			end,
		})
		:find()
end

return notmuch
