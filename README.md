# Telescope Notmuch Complete

This will extract email addresses from `notmuch` and present them using [Telescope](https://github.com/nvim-telescope/telescope.nvim).

Someone showed me [this](https://github.com/Valodim/deoplete-notmuch) which seemed pretty nifty. I was looking at Telescope at the time so thought I would take a run. It is useful when writing emails in mutt with the following options :

```
set edit_headers=yes # https://neomutt.org/guide/reference#edit_headers
set autoedit=yes # https://neomutt.org/guide/reference#autoedit
```

## Requirements

- [Telescope](https://github.com/nvim-telescope/telescope.nvim)
- [jq](https://stedolan.github.io/jq/)

## Installation

Using Packer:

```
    use {
        "https://codeberg.org/JoshuaCrewe/telescope-notmuch.nvim.git",
        config = function()
            require"telescope".load_extension("notmuch")
        end,
        ft = {'mail'}
    }
```

_Note: There does seem to be a performance hit when loading this plugin (vim boots slower), setting a filetype is a good way of limiting this._

The mapping which I use (dial x for [p]eople :

```
-- Create mappings with noremap option set to true
function map(mode, lhs, rhs, opts)
    local options = {noremap = true, silent = true}
    if opts then options = vim.tbl_extend('force', options, opts) end
    vim.api.nvim_set_keymap(mode, lhs, rhs, options)
end

map("i", "<c-x><c-p>", "<cmd>Telescope notmuch theme=cursor<CR>")
```

## Notmuch

Under the hood it runs the following command :

```
notmuch address --format=json ==deduplicate=address "*"
```
